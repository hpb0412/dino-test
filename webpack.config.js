const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');

const javascript = {
	test: /\.(js)$/ // see how we match anything that ends in `.js`? Cool
	, use: 'babel-loader'
};

const postcss = {
	loader: 'postcss-loader'
	, options: {
		plugins() { return [autoprefixer({ browsers: 'last 3 versions' })]; }
	}
};

const styles = {
	test: /\.(scss)$/
	, use: ExtractTextPlugin.extract({
		fallback: 'style-loader'
		, use: [
			'css-loader?sourceMap'
			, postcss
			, {
				loader: 'sass-loader?sourceMap'
				, options: {
					includePaths: [ path.resolve(__dirname, 'node_modules') ] 
				}
			}
		]
	})
};

module.exports = {
	entry: [
		'babel-polyfill'
		, './src/test-app.js'
	]

	, devtool: 'source-map'

	, output: {
		path: path.join(process.cwd(), 'src', 'public', 'js')
		, publicPath: '/'
		, filename: 'bundle.js'
	}

	, module: {
		rules: [
			javascript
			, styles
		]
	}

	, resolve: {
		extensions: [".js", ".json", ".css", ".scss"]
		, alias: {
			Containers: path.resolve(__dirname, 'src/components/containers')
			, Pures: path.resolve(__dirname, 'src/components/pures')
			, Ducks: path.resolve(__dirname, 'src/ducks')
			, Sass: path.resolve(__dirname, 'sass')
		}
	}
	
	, plugins: [
		new webpack.HotModuleReplacementPlugin()
		, new ExtractTextPlugin({
			filename: path.join(process.cwd(), 'src', 'public', 'css', 'style.css')
			, disable: process.env.NODE_ENV !== 'production'
		})
	]

	, devServer: {
		hot: true
		, historyApiFallback: true
		, contentBase: path.join(process.cwd(), 'src/public')
	}
};
