# Dino Test

## Setup
run command
`npm install`

or

`yarn install`


## Run
`npm start`

## Note
This project was design to run with mobile display first & only.
=> Please turn on devtool to simulate mobile view.
Tested on Chrome and Firefox.