import React from 'react';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute } from 'react-router';

import store, { history } from './store';
import App from './app';
import ResultPage from './pages/result';
import FriendDetailById from './containers/FriendDetailById.cont';

const AppRouter = () => {
	return (
		<Provider store={store}>
			<Router history={history}>
				<Route path='/' component={App}>
					<IndexRoute component={ResultPage} />
					<Route path=':id' component={FriendDetailById} />
				</Route>
			</Router>
		</Provider>
	);
}

export default AppRouter;
