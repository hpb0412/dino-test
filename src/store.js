import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import rootReducer from './ducks';

const middleware = [
	logger
	, thunk
];

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;


const store = createStore(rootReducer, /* initialState, */ composeEnhancers(
  applyMiddleware(...middleware)
));

// const store = createStore(rootReducer);

if (module.hot) {
  module.hot.accept('./ducks', () => {
    store.replaceReducer(require('./ducks').default);
  });
}

// export const history = syncHistoryWithStore(browserHistory, store);

export default store;
