import { combineReducers } from 'redux';

import db, * as dbapi from '../db';
import { FETCH_FRIENDS_SUCCEED, SELECT_FRIEND } from './action-types';

// SELECTOR(S)

const friendsSelectors = {
	getVisisbleFriends(friends, filter) {
		return friends.filter(friend => {
			const matchLocation = filter.location === '' || friend.location === filter.location;
			const matchGender = filter.gender === '' || friend.gender === filter.gender;
			const matchTags = filter.tags.every(tag => {
				return friend.tags.includes(tag);
			});

			return matchLocation && matchGender && matchTags;
		});
	}
}


// ACTION(S)

const friendsActions = {
	fetchFriends(filter) {
		const request = dbapi.fetchFriends(filter);
		// console.log('request', request);

		return (dispatch) => {
			return request
							.then(data => {
								console.log("data", data);
								dispatch({
									type: FETCH_FRIENDS_SUCCEED
									, payload: data
								});
							});
		}
	}

	,fetchAllFriends() {
		const request = dbapi.fetchAllFriends();
		// console.log('request', request);

		return (dispatch) => {
			return request
							.then(data => {
								console.log("data", data);
								dispatch({
									type: FETCH_FRIENDS_SUCCEED
									, payload: data
								});
							});
		}
	}
	
	,selectFriend(friend) {
		return {
			type: SELECT_FRIEND
			, payload: friend
		}
	}
}


// REDUCER(S)

const friendsReducer = (state = [], action) => {
	switch (action.type) {
		case FETCH_FRIENDS_SUCCEED:
			return action.payload;
		default:
			return state;
	}
	
}


export default {
	reducer: friendsReducer
	, actions: friendsActions
	, selectors: friendsSelectors
};
