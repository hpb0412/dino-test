
const FILTER_CHANGE = 'filter/FILTER_CHANGE';


// ACTION(S)

const filterActions = {
	changeFilter(newFilter) {
		return {
			type: FILTER_CHANGE
			, payload: newFilter
		}
	}
}


// REDUCER(S)

const initialFilter = {
	location: ''
	, gender: ''
	, tags: []
};

const filterReducer = (state = initialFilter, action) => {
	switch (action.type) {
		case FILTER_CHANGE:
			return action.payload;
		default:
			return state;
	}
}

export default {
	reducer: filterReducer
	, actions: filterActions
}
