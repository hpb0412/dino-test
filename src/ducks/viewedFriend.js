import { SELECT_FRIEND } from './action-types';



// REDUCER(S)

const viewedFriendReducer = (state = {}, action) => {
	switch (action.type) {
		case SELECT_FRIEND:
			return action.payload;
		default:
			return state;
	}
};


export default {
	reducer: viewedFriendReducer
}