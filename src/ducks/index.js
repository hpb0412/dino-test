import { combineReducers } from 'redux';

import friends from './friends';
import filter from './filter';
import modal from './modal';
import viewedFriend from './viewedFriend';
import follows from './follows';

// REDUCERS

export default combineReducers({
	friends: friends.reducer
	, filter: filter.reducer
	, modal: modal.reducer
	, viewedFriend: viewedFriend.reducer
	, follows: follows.reducer
});
