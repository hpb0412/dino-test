import { TOGGLE_FOLLOW } from './action-types';


// ACTION(S)

const followsActions = {
	toggleFollow(friendId) {
		return {
			type: TOGGLE_FOLLOW
			, payload: friendId
		}
	}
}


// REDUCER(S)

const followsReducer = (state = [], action) => {
	switch (action.type) {
		case TOGGLE_FOLLOW:
			let newState = [...state];
			if (newState.includes(action.payload)) {
				const index = newState.indexOf(action.payload);
				newState.splice(index, 1);
				return newState;
			}
			else {
				newState.push(action.payload);
				return newState;
			}
		default:
			return state;
	}
}

export default {
	reducer: followsReducer
	, actions: followsActions
}
