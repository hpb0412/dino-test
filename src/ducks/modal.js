import { SHOW_MODAL } from './action-types';

// ACTION(S)

const modalActions = {
	showModal(modalname) {
		return {
			type: SHOW_MODAL
			, payload: modalname
		}
	}
}



// REDUCER(S)

const modalReducer = (state = '', action) => {
	switch (action.type) {
		case SHOW_MODAL:
			return action.payload;
		default:
			return state;
	}
}

export default {
	reducer: modalReducer
	, actions: modalActions
};
