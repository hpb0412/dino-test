export const FETCH_FRIENDS = 'friends/FETCH_FRIENDS';
export const FETCH_FRIENDS_SUCCEED = 'friends/FETCH_FRIENDS_SUCCEED';
export const SELECT_FRIEND = 'friends/SELECT_FRIEND';
export const SHOW_MODAL = 'modal/SHOW_MODAL';
export const TOGGLE_FOLLOW = 'follows/TOGGLE_FOLLOW';

const CHANGE_LOCATION = 'filter/CHANGE_LOCATION';
const CHANGE_GENDER = 'filter/CHANGE_GENDER';
const CHANGE_TAGS = 'filter/CHANGE_TAGS';