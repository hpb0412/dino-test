import React, { Component } from 'react';
import { connect } from 'react-redux';

import FilterFormPure from 'Pures/FilterForm';
import modalDuck from 'Ducks/modal';
import filterDuck from 'Ducks/filter';

const { showModal } = modalDuck.actions;
const { changeFilter } = filterDuck.actions;

const mapStateToProps = (state, ownProps) => {
	return {
		show: state.modal === 'filter'
		, filter: state.filter
		, modal: state.modal
	}
}

export default connect(mapStateToProps, { showModal, changeFilter })(FilterFormPure);
