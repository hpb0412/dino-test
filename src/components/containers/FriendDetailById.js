import React, { Component } from 'react';
import { connect } from 'react-redux';

import FriendDetail from 'Pures/FriendDetail';
import friendsDuck from 'Ducks/friends';
import followsDuck from 'Ducks/follows';

const { selectFriend } = friendsDuck.actions;
const { toggleFollow } = followsDuck.actions;


class FriendDetailById extends Component {
	render() {
		const { friend, selectFriend, toggleFollow, followed } = this.props;

		return <FriendDetail friend={friend} onBackBtnClick={selectFriend} onToggleFollow={toggleFollow} followed={followed} />
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		friend: state.viewedFriend
		, followed: state.follows.includes(state.viewedFriend.id)
	}
}

export default connect(mapStateToProps, { selectFriend, toggleFollow })(FriendDetailById);
