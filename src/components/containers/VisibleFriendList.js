import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import FriendList from 'Pures/FriendList';
import friendsDuck from 'Ducks/friends';
import modalDuck from 'Ducks/modal';

const { fetchFriends, fetchAllFriends, selectFriend } = friendsDuck.actions;
const { showModal } = modalDuck.actions;

class VisibleFriendList extends Component {
	fetchAllData() {
		const { fetchAllFriends } = this.props;
		fetchAllFriends();
	}

	componentDidMount() {
		this.fetchAllData();
	}

	render() {
		const { friends, selectFriend, showModal } = this.props;

		return (
			<FriendList friends={friends}
									onFriendCardClick={selectFriend}
									onShowFilterClick={showModal}
			/>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		friends: friendsDuck.selectors.getVisisbleFriends(state.friends, state.filter)
		, filter: state.filter
	}
}

export default withRouter(connect(mapStateToProps, { fetchFriends, fetchAllFriends, selectFriend, showModal })(VisibleFriendList));
