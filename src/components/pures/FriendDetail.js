import React from 'react';
import { CSSTransitionGroup } from 'react-transition-group';

const FriendDetail = (props) => {
	const { friend, onBackBtnClick, onToggleFollow, followed } = props;

	if (Object.keys(friend).length === 0 && friend.constructor === Object) {
		return (
			<noscript />
		);
	}

	const { id, image, name, job, summary, tags } = friend;

	const infoTransitionOptions = {
		transitionName: 'info'
		, transitionAppear: true
		, transitionAppearTimeout: 500
		, transitionEnterTimeout: 500
		, transitionLeave: false
	};

	const summaryTransitionOptions = {
		transitionName: 'summary'
		, transitionAppear: true
		, transitionAppearTimeout: 500
		, transitionEnterTimeout: 500
		, transitionLeave: false
	};

	const tagsTransitionOptions = {
		transitionName: 'tags'
		, transitionAppear: true
		, transitionAppearTimeout: 500
		, transitionEnterTimeout: 500
		, transitionLeave: false
	}

	return (
		<div className="c-friend-detail">
			<div className="o-ratio o-ratio--4:3 c-friend-detail__img">
				<img className="o-ratio__content" src={image} />
			</div>

			<div className="c-friend-detail__content">
				<div className="c-friend-detail__header">
					<CSSTransitionGroup {...infoTransitionOptions}>
						<div className="c-friend-detail__info">
							<h4>{name}</h4>
							<p className="c-friend-detail__job">{job}</p>
						</div>
					</CSSTransitionGroup>
					<button className={`c-btn c-friend-detail__follow ${followed?'c-btn--primary':''}`}
									onClick={() => onToggleFollow(id)}>Follow</button>
				</div>

				<CSSTransitionGroup {...summaryTransitionOptions}>
					<div className="c-friend-detail__summary">
						<p>{summary}</p>
					</div>
				</CSSTransitionGroup>

				<div className="c-friend-detail__footer">
					<label className="c-friend-detail__label">Looking for</label>
					<CSSTransitionGroup {...tagsTransitionOptions}>
					{
						tags.length === 0 ? (
							<p>This friend did not look for anything</p>
						) : (
							<ul className="c-friend-detail__list">
								{tags.map((tag, i) => <li key={i}
																					className="c-friend-detail__item">{tag}</li>)}
							</ul>
						)
					}
					</CSSTransitionGroup>
				</div>
			</div>

			<button className="c-btn-back"
							onClick={() => {
								console.log("click", onBackBtnClick);
								onBackBtnClick({});
							}}> </button>
		</div>
	);
}

export default FriendDetail;
