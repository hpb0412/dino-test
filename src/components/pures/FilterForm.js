import React, { Component } from 'react';
import { CSSTransitionGroup } from 'react-transition-group';
import Select from 'react-select';

import ToggleGroup from './ToggleGroup';


const locations = [
	{ value: '', label: 'any place' }
	, { value: 'district 1', label: 'district 1' }
	, { value: 'district 2', label: 'district 2' }
	, { value: 'district 3', label: 'district 3' }
	, { value: 'district 4', label: 'district 4' }
];

const genders = [
	{ id:'guys', value: 'male', label: 'Guys' }
	,{ id:'girls', value: 'female', label: 'Girls' }
	,{ id:'both', value: '', label: 'Both' }
];

const tags = ['Asian', 'Sushi', 'Beer'];


class FilterForm extends Component {
	constructor(props) {
		super(props);

		this.state = props.filter;

		this.renderForm = this.renderForm.bind(this);
		this.onTagSelectChange = this.onTagSelectChange.bind(this);
	}

	onTagSelectChange(e) {
		let newTags = [...this.state.tags];
		const value = e.target.value;
		
		if (newTags.includes(value)) {
			const index = newTags.indexOf(value);
			newTags.splice(index, 1);

			this.setState({
				tags: newTags
			});
		}
		else {
			this.setState({
				tags: [...this.state.tags, value]
			})
		}
	}

	renderForm() {
		const { filter, showModal, changeFilter } = this.props;
		

		return (
			<form className="c-filter-form" key="filter-form">
				<div className="c-filter-form__field">
					<h5 className="c-filter-form__field-header">Location</h5>
					<Select
						name="location"
						value={this.state.location}
						options={locations}
						clearable={false}
						searchable={false}
						onChange={(el) => this.setState({ location:el.value })}
					/>
				</div>

				<div className="c-filter-form__field">
				<h5 className="c-filter-form__field-header">Show me</h5>
					<ToggleGroup
						name="gender"
						value={this.state.gender}
						options={genders}
						onChange={(value) => this.setState({ gender: value })}
					/>
				</div>

				<div className="c-filter-form__field">
					{
						tags.map((tag, i) => {
							const id = `tag_${i}`;
							return (
								<span key={id} className="c-tag">
									<input type="checkbox"
													checked={this.state.tags.includes(tag)}
													id={id} name="tags"
													value={tag}
													onChange={this.onTagSelectChange}
									/>
									<label htmlFor={id}>{tag}</label>
								</span>
							);
						})
					}
				</div>
				
				<input type="submit"
								className={this.state === this.props.filter ? "c-filter-form__btn-close": "c-filter-form__btn-save"}
								value=""
								onClick={(e) => {
									e.preventDefault();
									showModal('');
									changeFilter({...this.state});
								} }
				/>
			</form>
		);
	}

	render() {
		const { show } = this.props;

		if (!show) {
			return (
				<noscript />
			);
		}

		const transitionOptions = {
			transitionName: "up"
			, transitionAppear: true
			, transitionAppearTimeout: 500
			, transitionEnterTimeout: 500
			, transitionLeaveTimeout: 500
		}		

		const form = this.renderForm();

		return (
			<div className="o-modal">
				<div className="o-modal__content">
					<CSSTransitionGroup {...transitionOptions}>
						{form}
					</CSSTransitionGroup>
				</div>
			</div>
		);
	}
}

export default FilterForm;
