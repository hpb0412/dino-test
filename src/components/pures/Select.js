import React, { Component } from 'react';


class Select extends Component {
	constructor(props) {
		super(props);

		this.state = {
			selectedOption: props.value
			, dropped: false
		}

		this.handleOptionChange = this.handleOptionChange.bind(this);
	}

	// getDefaultProps() {

	// }

	handleOptionChange(e) {
		this.setState({
			selectedOption: e.target.value
		});
	}

	render() {
		const { name, options, onChange } = this.props;

		return (
			<div className="c-select">
				<input type="hidden" name={name} />
				<div className="c-select__controls">
					<span className="c-select__value">Value</span>
					<button className="c-select__arrow"
									onClick={() => this.setState({ dropped: !this.state.dropped }) }>⬇</button>
				</div>
				{ this.state.dropped &&
					<ul className="c-select__list">
					{
						options.map((opt, i) => {
							return (
								<li key={i} className="c-select__item">
									{opt.label}
								</li>
							);
						})
					}
					</ul>
				}
			</div>
		);
	}
}

export default Select;
