import React from 'react';


const FriendCard = (props) => {

	const { id, name, image, onClick } = props;
	return (
		<li className="o-layout__item  u-1/3 c-friend-card--translated"
				onClick={onClick}>
			<div className="c-friend-card">
				<img className="c-friend-card__img" src={image} />
				<h2 className="c-friend-card__body">{name}</h2>
			</div>
		</li>
	);
}

export default FriendCard;
