import React, { Component } from 'react';


class ToggleGroup extends Component {
	constructor(props) {
		super(props);

		this.handleOptionChange = this.handleOptionChange.bind(this);
	}

	handleOptionChange(e) {
		this.props.onChange(e.target.value);
	}

	render() {
		const { name, value, options } = this.props;

		return (
			<div className="c-toggle-group">
				<ul className="c-toggle-group__wrapper">
				{
					options.map((opt, i) => {
						return (
							<li key={i} className="c-toggle-group__btn">
								<input type="radio"
												className="c-toggle-group__input"
												id={opt.id}
												name={name}
												value={opt.value}
												onChange={this.handleOptionChange}
												checked={value === opt.value}
								/>
								<label htmlFor={opt.id}
												className="c-toggle-group__label">
									{opt.label}
								</label>
							</li>
						);
					})
				}
				</ul>
			</div>
		);
	}
}

export default ToggleGroup;
