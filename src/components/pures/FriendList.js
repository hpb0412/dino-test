import React from 'react';
import { CSSTransitionGroup } from 'react-transition-group';

import FriendCard from './FriendCard';

const FriendList = (props) => {
	const { friends, onFriendCardClick, onShowFilterClick } = props;

	if (friends.length === 0) {
		return (
			<div className="o-wrapper">
				<p>No Friends Found</p>
				<button className="c-btn-filter"
							onClick={() => onShowFilterClick('filter')}></button>
			</div>
		);
	}

	const transitionOptions = {
		transitionName: "scale"
		, transitionAppear: true
		, transitionAppearTimeout: 300
		, transitionEnterTimeout: 300
		, transitionLeaveTimeout: 300
	}

	return (
		<div className="o-wrapper">
			<p className="c-friend-list__title">Result</p>
			<ul className="o-layout c-friend-list__content">
				<CSSTransitionGroup {...transitionOptions}>
				{friends.map(f =>
					<FriendCard key={f.id} {...f}
											onClick={() => {
												console.log('clicked');
												console.log(onFriendCardClick);
												onFriendCardClick(f);
											}}/>)}
					</CSSTransitionGroup>
			</ul>

			<button className="c-btn-filter"
							onClick={() => onShowFilterClick('filter')}></button>
		</div>
	);

}

export default FriendList;