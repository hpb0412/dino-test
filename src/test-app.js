import 'Sass/main.scss';

import React from 'react';
import ReactDOM from 'react-dom';

import App from './app';

document.addEventListener('DOMContentLoaded', function() {
	ReactDOM.render(
		<App />
		, document.querySelector('#app')
	);
});

if (module.hot) {
  module.hot.accept('./app');
}
