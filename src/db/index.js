import { v4 } from 'uuid';


const girls = [
	{
		id: v4()
		, name: 'Sophia'
		, gender: 'female'
		, image: 'https://randomuser.me/api/portraits/women/94.jpg'
		, summary: 'Sit culpa velit deserunt excepteur veniam nostrud id elit ex deserunt adipisicing officia ad.'
		, job: 'job title'
		, location: 'district 1'
		, tags: ['Asian', 'Sushi'] 
	}
	,{
		id: v4()
		, name: 'Amellia'
		, gender: 'female'
		, image: 'https://randomuser.me/api/portraits/women/57.jpg'
		, summary: 'Lorem laborum proident consequat eiusmod veniam sunt laborum cillum.'
		, job: 'job title'
		, location: 'district 2'
		, tags: ['Asian', 'Beer'] 
	}
	,{
		id: v4()
		, name: 'Ora'
		, gender: 'female'
		, image: 'https://randomuser.me/api/portraits/women/60.jpg'
		, summary: 'Cupidatat consequat minim duis sunt eiusmod.'
		, job: 'job title'
		, location: 'district 3'
		, tags: ['Sushi'] 
	}
	,{
		id: v4()
		, name: 'Stelia'
		, gender: 'female'
		, image: 'https://randomuser.me/api/portraits/women/90.jpg'
		, summary: 'Consectetur et Lorem in nostrud quis mollit esse aliquip magna laborum sunt consequat.'
		, job: 'job title'
		, location: 'district 4'
		, tags: ['Beer'] 
	}
	,{
		id: v4()
		, name: 'Edna'
		, gender: 'female'
		, image: 'https://randomuser.me/api/portraits/women/32.jpg'
		, summary: 'Sunt quis fugiat eiusmod deserunt id id ullamco irure nostrud.'
		, job: 'job title'
		, location: 'district 1'
		, tags: ['Asian', 'Sushi'] 
	}
	,{
		id: v4()
		, name: 'Rosa'
		, gender: 'female'
		, image: 'https://randomuser.me/api/portraits/women/82.jpg'
		, summary: 'In officia ullamco minim qui enim voluptate anim velit labore nulla reprehenderit aliqua pariatur.'
		, job: 'job title'
		, location: 'district 2'
		, tags: ['Asian', 'Beer'] 
	}
	,{
		id: v4()
		, name: 'Iva'
		, gender: 'female'
		, image: 'https://randomuser.me/api/portraits/women/63.jpg'
		, summary: 'Irure eu proident in fugiat ex incididunt et ad.'
		, job: 'job title'
		, location: 'district 3'
		, tags: ['Sushi'] 
	}
	,{
		id: v4()
		, name: 'Helen'
		, gender: 'female'
		, image: 'https://randomuser.me/api/portraits/women/73.jpg'
		, summary: 'Deserunt nostrud voluptate proident enim fugiat.'
		, job: 'job title'
		, location: 'district 4'
		, tags: ['Beer'] 
	}
];

const boys = [
	{
		id: v4()
		, name: 'Brian'
		, gender: 'male'
		, image: 'https://randomuser.me/api/portraits/men/85.jpg'
		, summary: 'Sit culpa velit deserunt excepteur veniam nostrud id elit ex deserunt adipisicing officia ad.'
		, job: 'job title'
		, location: 'district 1'
		, tags: ['Asian', 'Sushi'] 
	}
	,{
		id: v4()
		, name: 'Craig'
		, gender: 'male'
		, image: 'https://randomuser.me/api/portraits/men/74.jpg'
		, summary: 'Lorem laborum proident consequat eiusmod veniam sunt laborum cillum.'
		, job: 'job title'
		, location: 'district 2'
		, tags: ['Asian', 'Beer'] 
	}
	,{
		id: v4()
		, name: 'Alvin'
		, gender: 'male'
		, image: 'https://randomuser.me/api/portraits/men/33.jpg'
		, summary: 'Cupidatat consequat minim duis sunt eiusmod.'
		, job: 'job title'
		, location: 'district 3'
		, tags: ['Sushi'] 
	}
	,{
		id: v4()
		, name: 'David'
		, gender: 'male'
		, image: 'https://randomuser.me/api/portraits/men/36.jpg'
		, summary: 'Consectetur et Lorem in nostrud quis mollit esse aliquip magna laborum sunt consequat.'
		, job: 'job title'
		, location: 'district 4'
		, tags: ['Beer'] 
	}
	,{
		id: v4()
		, name: 'Frank'
		, gender: 'male'
		, image: 'https://randomuser.me/api/portraits/men/32.jpg'
		, summary: 'Sunt quis fugiat eiusmod deserunt id id ullamco irure nostrud.'
		, job: 'job title'
		, location: 'district 1'
		, tags: ['Asian', 'Sushi'] 
	}
	,{
		id: v4()
		, name: 'Gale'
		, gender: 'male'
		, image: 'https://randomuser.me/api/portraits/men/88.jpg'
		, summary: 'In officia ullamco minim qui enim voluptate anim velit labore nulla reprehenderit aliqua pariatur.'
		, job: 'job title'
		, location: 'district 2'
		, tags: ['Asian', 'Beer'] 
	}
	,{
		id: v4()
		, name: 'Hale'
		, gender: 'male'
		, image: 'https://randomuser.me/api/portraits/men/28.jpg'
		, summary: 'Irure eu proident in fugiat ex incididunt et ad.'
		, job: 'job title'
		, location: 'district 3'
		, tags: ['Sushi'] 
	}
	,{
		id: v4()
		, name: 'Ivan'
		, gender: 'male'
		, image: 'https://randomuser.me/api/portraits/men/75.jpg'
		, summary: 'Deserunt nostrud voluptate proident enim fugiat.'
		, job: 'job title'
		, location: 'district 4'
		, tags: ['Beer'] 
	}
];

const fakeDB = {
	girls
	,boys
	,both: [
		...girls,
		...boys
	]
};

export default fakeDB;


const filterByGender = (gender) => {
	switch (gender) {
		case 'female':
			return fakeDB.girls;
		case 'male':
			return fakeDB.boys;
		default:
			return fakeDB.both;
	}
}

const filterByLocation = (friends, location) => {
	return friends.filter(f => {
		return (location === '' || f.location === location);
	});
}

const filterByTags = (friends, tags) => {
	return friends.filter(f => {
		return tags.every(tag => f.tags.includes(tag));
	});
}

const delay = (ms) => {
 	return new Promise(resolve => setTimeout(resolve, ms));
}

export const fetchAllFriends = () => {
	return delay(500)
		.then(() => {
			return fakeDB.both;
		});
}

export const fetchFriends = (filter) => {
	console.log("filter", filter);
	return delay(500)
		.then(() => {
			
			const byGender = filterByGender(filter.gender);
			// console.log("byGender", byGender);
			const byLocation = filterByLocation(byGender, filter.location);
			// console.log("byLocation", byLocation);
			const byTags = filterByTags(byLocation, filter.tags);
			// console.log("byTags", byTags);
			return byTags;
		});
}
