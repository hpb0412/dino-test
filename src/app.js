import React, { Component } from 'react';
import { Provider } from 'react-redux';
// import { Router, Route, IndexRoute } from 'react-router';

import store, { history } from './store';

import VisibleFriendList from 'Containers/VisibleFriendList';
import FriendDetailById from 'Containers/FriendDetailById';
import FilterForm from 'Containers/FilterForm';


class App extends Component {
	

	render() {
		return (
			<Provider store={store}>
				<div>

					<VisibleFriendList />

					<FriendDetailById />
				
					<FilterForm />
				
				</div>
			</Provider>
		);
	}
}

export default App;
